var Storage = (function () {
    var _st = window.localStorage;
    var cache, dirty;
    var requestData = function () {
        let data = _st.getItem("rmtt-storage");

        if (!data) {
            data = {};
            _st.setItem(JSON.stringify(data));
        }

        return data;
    };

    var getStorage = function () {
        if (!cache) {
            cache = requestData();
        }

        return cache;
    };

    var updateStorage = function () {
        if (cache && dirty) {
            dirty = false;
            _st.setItem(JSON.stringify(cache));
        }
    };

    var startTimer = function (timer) {
        let storage = getStorage(),
            tId = timer.id;
        if (!storage.timers) {
            storage.timers = {};
        }

        stopTimers();

        storage.timers[tId] = {
            ts: timer.ts,
            active: true
        };

        storage.active = tId;
    };

    var stopTimerById = function (id) {
        let storage = getStorage();
        if (!storage.timers) {
            storage.timers = {};
        }
        let sTimers = storage.timers;

        if (!sTimers[id]) {
            return;
        }

        stopTimer(sTimers[id]);

        if (storage.active === id) {
            storage.active = null;
        }
    };

    var stopTimer = function (timer) {
        let time = timer.time ? +timer.time : 0;
        let ts = timer.ts ? +timer.ts : 0;

        if (ts) {
            time += (Date.now() - ts)/1000;
        }

        timer.active = false;
        timer.time = time;
        timer.ts = 0;
    };
    
    var stopTimers = function () {
        let storage = getStorage();
        if (!storage.timers) {
            storage.timers = {};
        }
        let sTimers = storage.timers;

        Object.keys(sTimers).forEach(one => stopTimer(one));
        storage.active = null;
    };

    var getActiveTimer = function () {
        let storage = getStorage();
        if (!storage.timers) {
            storage.timers = {};
        }

        return storage.active && storage.timers[storage.active];
    };

    setInterval(updateStorage, 2000);

    return {
        start: startTimer,
        stop: stopTimerById,
        getTimer: getActiveTimer
    };
})();
