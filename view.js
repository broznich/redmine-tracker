// jshint esversion:6
const elements = {
    "rmtt-start-btn": { tag: "a", cls: "rmtt-button", action: "start" },
    "rmtt-stop-btn": { tag: "a", cls: "rmtt-button", action: "stop" },
    "rmtt-pause-btn": { tag: "a", cls: "rmtt-button", action: "pause" },
};

const css = "a.rmtt-button {display:inline-block;width:16px;height:16px;border: 1px solid black; margin: 2px;}";

let View = function (cont) {
    this.baseContainer = cont;
    this.baseContainer.innerHTML = "";
    this.init();
    
};

View.prototype = {
    init: function () {
        let els, container, cssEl;
        container = this.createElement({
            tag: "div",
            cls: "rmtt-base"
        });

        cssEl = this.createElement({
            tag: "style"
        });

        cssEl.textContent = css;

        els = Object.keys(elements);

        els.forEach(one => container.appendChild(this.createElement(elements[one])));

        this.container = container;
        this.baseContainer.appendChild(this.container);
        this.baseContainer.appendChild(cssEl);
    },

    createElement: function (cfg) {
        let el = document.createElement(cfg.tag);

        if (cfg.cls) {
            el.className = cfg.cls;
        }

        switch (cfg.tag) {
            case "a":
                el.href = "#";
                el.setAttribute("data-action", "cfg.action");
                el.addEventListener("click", this.buttonAction.bind(this, cfg.action));
                break;
        }

        return el;
    },

    buttonAction: function (action, event) {
        event.preventDefault();
        console.log(action);
    }
};

let view = new View();