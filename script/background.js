/* globals chrome, RMAPI */

class App {
    constructor () {
        this.api = this.createApi();
        this.createNFListener();
        this.initSyncer();
    }

    initSyncer() {
        this.snc = setInterval(this.sync.bind(this),3000);
    }

    createApi () {
        return new RMAPI({
            apiKey: localStorage.getItem("apiKey"),
            basePath: localStorage.getItem("path")
        });
    }

    createNFListener () {
        chrome.runtime.onMessage.addListener(this.messageHandler.bind(this));
    }

    messageHandler (request, sender, sendResponse) {
        this.requestAction(request, sendResponse);
        return true;
    }

    requestAction (request, callback) {
        let action = request.action;

        switch (action) {
            case "status":
                this.doStatus(callback);
                break;
            case "timer":
                this.doTimer(request.data, callback);
                break;
            default:
                return null;
        }
    }

    doStatus (callback) {
        let self = this;
        this.api.getStatus(function(status) {
            self.getCurrentTask(function (task) {
                status.current = task;
                status.saved = self.getSavedTask();
                
                callback(status);
            });
        });
    }

    getSavedTask () {
        return localStorage.getItem("task");
    }

    getCurrentTask (callback) {
        let task = this.getSavedTask();

        if (task) {
            return callback(task);
        }

        chrome.tabs.getSelected(function (tab) {
            let url;
            if (tab) {
                url = tab.url.match(/issues\/([\d]+)/);
            }
            
            callback(url ? url[1] : null);
        });
    }

    sync () {
        debugger;
        this.doStatus(function (status) {
            if (status.task) {
                let converted = status.task.replace("#","");
                if (converted !== localStorage.getItem("task")) {
                    localStorage.setItem("task", converted);
                }
            }
        });
    }

    doTimer (data, callback) {
        let self = this;
        let action = data.action;
        
        switch (action) {
            case "start":
                this.getCurrentTask(function (task) {
                    if (!task) {
                        return callback(null);
                    }

                    self.startTask(task, function (status) {
                        if (status) {
                            localStorage.setItem("task", task);
                        }

                        callback({status: status, task: task});
                    });
                });
                break;
            case "stop":
                this.stopTask(function (status) {
                    if (status) {
                        localStorage.removeItem("task");
                    }
                    callback({status: status});
                });
                break;
            case "pause":
                this.stopTask(function (status) {
                    callback({status: status});
                });
                break;
        }
    }

    startTask (task, callback) {
        this.api.start(task, callback);
    }

    stopTask (callback) {
        this.api.stop(callback);
    }
}

window.RMTTApp = new App();