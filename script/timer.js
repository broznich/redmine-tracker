var Timer = function () {
    let sTime = +localStorage.getItem("sTime");
    this._sTime = isNaN(sTime) ? 0 : sTime;
};

Timer.prototype = {
    start: function () {
        this._sTime = Date.now();
        localStorage.setItem("sTime", this._sTime);
    },

    stop: function () {
        let time = this._sTime;
        this._sTime = 0;
        localStorage.setItem("sTime", this._sTime);

        return Date.now() - time;
    },

    getTime: function () {
        let now = Date.now();

        return now - (this._sTime || now);
    },

    format: function (ts, nosecond, splitter) {
        var timeInSec = Math.round(ts/1000);
        splitter = splitter || ':'; 
        
        var hours = Math.floor(timeInSec/3600);
        var othersSec = timeInSec % 3600;
        var mins = Math.floor(othersSec/60);
        var secs = othersSec % 60;
            
        var time = hours + splitter + (mins >= 10 ? mins : '0' + mins);
        
        if (!nosecond) {
            time += splitter + (secs >= 10 ? secs : '0' + secs);
        }
        
        return time;
    },
};
