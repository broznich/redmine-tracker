// jshint esversion:6
window.RMAPI = function (options) {
    // getCurrentTimer
    // getTimers
    // startTimer
    // stopTimer
    // deleteTimer



    var base = options.basePath;//"https://rm.innomdc.com/";
    var apiKey = options.apiKey;

    // var buildJSONUrl = function (path) {
    //     return base + path + ".json";
    // };

    var buildUrl = function (path) {
        return base + path;
    };

    var request = function (params, callback) {
        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;
        console.log(params.url);
        xhr.open('GET', params.url, true);
        xhr.setRequestHeader("X-Redmine-API-Key", params.key);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                callback(this);
            }
        };
        xhr.send(null);
    };

    var getTimers = function (data) {
        var dp = new DOMParser();
        var dom = dp.parseFromString(data, "text/html");
        var list = dom.querySelectorAll("#user-time-loggers-list tr");
        var result = [];
        // without headers

        for (let i = 1, iLen = list.length; i < iLen; i++) {
            let item = list[i];
            let id = item.querySelector(".buttons a").getAttribute("href").match(/\d+$/);
            let issue = item.querySelector(".issue a").getAttribute("href").match(/\d+$/);

            result.push({
                id: id[0],
                issue: issue[0]
            });
        }

        return result;
    };

    var getAllTimers = function (callback) {
        request({
            url: buildUrl("time_loggers"),
            key: apiKey
        }, function (rsp) {
            callback(getTimers(rsp.responseText));
        });
    };

    var getCurrentTimer = function (callback) {
        request({
            url: buildUrl("time_loggers/render_menu?_=" + Date.now()),
            key: apiKey    
        }, function (rsp) {
            callback(rsp.responseText.match(/href="\/issues\/(\d+)"/)[1]);
        });
    };

    var startTimer = function (issueId, callback) {
        request({
            url: buildUrl("time_loggers/start?issue_id=" + issueId),
            key: apiKey
        }, function (rsp) {
            callback(rsp.status === 200);
        });
        //https://rm.innomdc.com/time_loggers/start?issue_id=11768
    };

    var stopTimer = function (callback) {
        request({
            url: buildUrl("time_loggers/stop"),
            key: apiKey    
        }, function (rsp) {
            callback(rsp.status === 200);
        });
    };

    var getStatus = function (callback) {
        request({
            url: buildUrl("time_loggers/render_menu?_=" + Date.now()),
            key: apiKey
        }, function (rsp) {
            if (rsp.status !== 200) {
                return callback(null);
            }

            var dp = new DOMParser();
            var dom = dp.parseFromString(rsp.responseText, "text/html");

            var isRunned = dom.querySelector("span.icon-time");
            var task = dom.querySelector("a.icon:first-child");
            var start = dom.querySelector("a.icon-start-action");
            var stop = dom.querySelector("a.icon-stop-action");
            var time = dom.body.textContent.match(/\/[^/]+\//);

            if (isRunned && isRunned.textContent.trim() === "Not running") {
                return callback({
                    status: "stopped"
                });
            }

            return callback({
                task: task ? task.textContent : null,
                time: time ? time[0].replace(/\//g,"").trim() : null,
                status: start && stop ? "paused" : "started"
            });
        });
    };


    /*
    request({
        url: buildUrl("time_loggers"),
        key: apiKey    
    }, function (rsp) {
        console.log(getTimers(rsp.responseText));
    });
    */

    return {
        start: startTimer,
        stop: stopTimer,
        getCurrentTimer: getCurrentTimer,
        getAllTimers: getAllTimers,
        getStatus: getStatus
    };
};
