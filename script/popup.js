/* globals chrome */
class Popup {
    constructor () {
        this.initElements();
        this.updateStatus();
    }

    initElements () {
        let views = {};

        views.start = document.querySelector("#main .time-bb .button.start-button");
        views.stop = document.querySelector("#main .time-bb .button.stop-button");
        views.pause = document.querySelector("#main .time-bb .button.pause-button");
        views.task = document.querySelector("#main .time-task");
        views.timer = document.querySelector("#main .time-timer");

        views.start.addEventListener("click", this.doButtonAction.bind(this));
        views.stop.addEventListener("click", this.doButtonAction.bind(this));
        views.pause.addEventListener("click", this.doButtonAction.bind(this));

        this.views = views;
    }

    request (action, data, callback) {
        if (!callback) {
            callback = data;
            data = null;
        }

        chrome.runtime.sendMessage({ action: action, data: data }, callback);
    }

    hideAllButtons () {
        this.setButtonVisibility(this.views.start);
        this.setButtonVisibility(this.views.stop);
        this.setButtonVisibility(this.views.pause);        
    }

    setButtonVisibility(button, visible) {
        button.style.display = visible ? "" : "none";
    }
 
    setStatus (status = "stopped") {
        let views = this.views;

        this.hideAllButtons();
        switch (status) {
            case "started":
                this.setButtonVisibility(views.start);
                this.setButtonVisibility(views.stop, true);
                this.setButtonVisibility(views.pause, true);
                break;
            case "paused":
                this.setButtonVisibility(views.start, true);
                this.setButtonVisibility(views.stop, true);
                this.setButtonVisibility(views.pause);
                break;
            case "stopped":
                this.setButtonVisibility(views.start, true);
                this.setButtonVisibility(views.stop);
                this.setButtonVisibility(views.pause);
                break;
        }
    }

    setTask (task = "") {
        this.views.task.textContent = task;
    }

    setTimer (time = "") {
        this.views.timer.textContent = time;
    }

    updateStatus () {
        let self = this;
        this.request("status", function (data) {
            if (!data) {
                data = {};
            }

            self.setStatus(data.status === "stopped" && data.saved ? "paused" : data.status);
            self.setTask(data.task || ("#" + data.current));
            self.setTimer(data.time);
        });
    }

    doButtonAction (event) {
        let el = event.target,
            action = el.getAttribute("data-action");


        console.log(action);
        this.request("timer", {action: action}, this.updateStatus.bind(this));
    }
}

let popup = new Popup();