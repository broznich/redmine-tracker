var saveOptions = function () {
    var keys = this.querySelectorAll("input[type=text]") || [];

    for (let i=0, iLen = keys.length; i < iLen; i++) {
        let one = keys[i];
        localStorage.setItem(one.name, one.value);
    }
};

document.getElementById("options").addEventListener("submit", saveOptions);